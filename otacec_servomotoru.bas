rem Otacec servomotoru
start:
	rem Otoc servomotor na pinu C.1 do 1. krajni polohy
	servo C.1,100
	rem Pockej 1.0 s
	pause 1000
	rem Otoc servomotor na pinu C.1 do 2. krajni polohy
	servo C.1,200
	rem Pockej 2.0 s
	pause 2000
	rem Vrat se na zacatek
	goto start
