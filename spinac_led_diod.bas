rem Spínač LED diody na portu C.1
start:
	#rem
		Když je na pinu C.3 proud rozsvit LED diodu na pinu C.1,
		pokud není na pinu C.3 proud zhasní LED diodu na pinu C.1.
	#endrem
	if C.3 = 1 then
		rem Rozsvit LED diodu na pinu C.1
		high C.1
	else
		rem Zhasni LED diodu na pinu C.1
		low C.1
	endif
	rem Vrat se na start
	goto start
