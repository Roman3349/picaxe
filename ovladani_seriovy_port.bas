rem Ovladani LED diody pomocí serioveho portu
start:
	serrxd b0
	rem Kdyz je odeslan prikaz 1 rozsvit LED diodu na portu C.4
	if b0 = 1 then
		high C.4
	rem Kdyz je odeslan prikaz 0 zhasni diodu na portu C.4
	else if b0 = 0 then
		low C.4
	endif
	goto start