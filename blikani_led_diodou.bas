REM Blikání LED diodou na portu C.1
start:
	rem Zmen stav portu C.1
	toggle C.1
	rem Počkej 0.5 s
	pause 500
	rem Vrat se na start
	goto start
